
ARG version=16
FROM node:${version}

COPY . /app
WORKDIR /app
RUN npm install 

ARG username=no_root_user
ARG groupname=no_root_group

RUN useradd -ms /bin/bash ${username}
RUN chown -R ${username}:${username} /app
RUN chmod 755 /app

USER $username

ENV TEST_ENV="Hello, im an enviroment variable"
ENV PORT=5000

HEALTHCHECK --interval=5m --timeout=3s \
  CMD curl -f http://localhost:${PORT}/healthz || exit 1

EXPOSE 8000

CMD node -v && whoami && npm start
