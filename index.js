const express = require('express')
const app = express()
const port =  process.env.PORT || 33010
const some_variable = process.env.TEST_ENV
const uuid = require('uuid')

app.get('/', (req, res) => {
  res.send(`its your testing .env variable: ${some_variable}`)
})

app.get("/healthz", (req,res)=>{
  res.send(`Everything is okay, dont worry, time now is ${new Date().toDateString()}`)
})

app.get("/uuid", (req, res)=>{
  res.send(JSON.stringify({
    uuid: uuid.v4()
  }))
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})
