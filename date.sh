#!/bin/bash

echo "Current date: $(date)"
docker build --tag homework_20240208 --build-arg version=latest --build-arg username=vladlen --build-arg groupname=group_vladlen . 
docker run -v ./date.sh:/container/mounted/file --name homerwork_container --rm  -it -p 8000:5000 homework_20240208

 